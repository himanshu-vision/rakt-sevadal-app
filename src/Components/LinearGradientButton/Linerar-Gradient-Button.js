import React from 'react';
import { Text, TouchableOpacity, StyleSheet, Dimensions, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import COLOR from '../../Constants/color';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
	buttonText: {
		textAlign: 'center',
		color: COLOR.PRIMARY,
		fontWeight: '700',
		color: '#fff',
		fontSize: 20,
	},
	buttonContainer: {
		width,
		height: 70,
		alignItems: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		bottom: 0,
        position: 'absolute',
		backgroundColor: COLOR.SECONDARY,
	},
});

const LinearGradientButton = ({ title, onPress, isProcessing }) => (
	<TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
		{isProcessing ? (
			<ActivityIndicator animating={true} color="#fff" size="large" />
		) : (
			<Text style={styles.buttonText}>{title}</Text>
		)}
	</TouchableOpacity>
);

export default LinearGradientButton;
