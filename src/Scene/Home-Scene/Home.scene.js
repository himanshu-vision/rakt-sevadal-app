import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	Image,
	TouchableNativeFeedback,
	TextInput,
	Platform,
	PermissionsAndroid,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView, { LocalTile, Marker } from 'react-native-maps';
import isEqual from 'lodash/isEqual';

// import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';
import { UpdateUserLocationCall } from '../../api/private';

LAT_DELTA = 0.062196068465709686;
LNG_DELTA = 0.10778672928606525;

const GEOLOCATION_OPTIONS = { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 };
const ANCHOR = { x: 0.5, y: 0.5 };

const { width, height } = Dimensions.get('window');

export default class HomeScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
			myPosition: null,
			addressPosition: null,
			formattedAddress: '',
			locationObject: null,
		};
	}

	onRegionChange = region => {
		this.setState({ addressPosition: region });
	};

	componentDidMount() {
		this.mounted = true;
		// MessageBarManager.showAlert({
		// 	message: 'Getting your location ...',
		// 	type: 'warning',
		// });
		// if (this.state.locationObject) {
		// 	this.animateTo(this.state.locationObject.latitude, this.state.locationObject.longitude);
		// }
		// If you supply a coordinate prop, we won't try to track location automatically
		if (Platform.OS === 'android') {
			PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(granted => {
				if (granted) this.watchLocation();
			});
		} else {
			this.watchLocation();
		}
	}

	animateTo = (latitude, longitude) => {
		this.map.animateToRegion(
			{
				latitude,
				longitude,
				latitudeDelta: LAT_DELTA,
				longitudeDelta: LAT_DELTA,
			},
			1000
		);
	};

	watchLocation = () => {
		navigator.geolocation.getCurrentPosition(
			position => {
				const myPosition = position.coords;
				const { latitude, longitude } = myPosition;
				this.map.animateToRegion(
					{
						latitude,
						longitude,
						latitudeDelta: LAT_DELTA,
						longitudeDelta: LAT_DELTA,
					},
					1000
				);
				this.setState({ isProcessing: false });
				this.gotLocation(myPosition);
			},
			error => console.log(error, 'ERROR LOCATION'),
			GEOLOCATION_OPTIONS
		);
	};

	gotLocation = async position => {
		const { latitude, longitude } = position;
		const url = `https://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=en&latlng=${latitude},${longitude}&key=AIzaSyB1Xe6BJLFiueprlpRsSw0a67VWvdmMuHc`;
		const response = await fetch(url, { method: 'GET' });
		const res = await JSON.parse(response._bodyInit);
		if (res.status == 'OK') {
			const address = res.results[0];
			const locationObject = {
				latitude,
				longitude,
				formatted_address: address.formatted_address,
				address_components: address.address_components,
				place_id: address.place_id,
			};
			this.setState({ formattedAddress: address.formatted_address, locationObject });
			UpdateUserLocationCall({ location: locationObject });
		}
	};

	render() {
		let { coordinate, token } = this.props;
		const { formattedAddress } = this.state;

		return (
			<View style={styles.container}>
				<TextInput
					placeholder={'Getting your location ...'}
					style={{
						padding: 10,
						backgroundColor: '#fff',
						width: width - 20,
						height: 50,
						elevation: 4,
						borderRadius: 4,
						position: 'absolute',
						top: 10,
					}}
					value={formattedAddress}
					underlineColorAndroid={'rgba(0,0,0,0)'}
					onChangeText={formattedAddress => this.setState({ formattedAddress })}
				/>
				<MapView
					loadingEnabled={this.state.isProcessing}
					ref={ref => {
						this.map = ref;
					}}
					style={styles.map}
					showsUserLocation
					userLocationAnnotationTitle="current location"
					followsUserLocation
					showsMyLocationButton
					onRegionChange={region => this.onRegionChange(region)}
				/>
				<Image
					source={require('../../Assets/Images/pin.png')}
					style={{ width: 40, height: 40, resizeMode: 'contain', marginBottom: -40 }}
				/>
				<TouchableNativeFeedback
					onPress={() => {
						if (this.state.locationObject) {
							Actions.search_blood_doner({ locationObject: this.state.locationObject, token });
						} else {
							MessageBarManager.showAlert({
								message: 'Wait while getting your location ...',
								type: 'warning',
							});
						}
					}}
				>
					<View style={styles.buttonCircle}>
						<Image
							source={require('../../Assets/Images/white-search.png')}
							style={{ margin: 5, width: 25, height: 25 }}
						/>
						<Text style={styles.textOne}>Search Blood Doners</Text>
					</View>
				</TouchableNativeFeedback>
				<TouchableNativeFeedback onPress={() => this.componentDidMount()}>
					<View
						style={{
							width: 50,
							height: 50,
							borderRadius: 30,
							backgroundColor: '#fff',
							alignItems: 'center',
							justifyContent: 'center',
							borderColor: COLORS.SECONDARY,
							borderWidth: 1,
							position: 'absolute',
							bottom: 70,
							right: 20,
						}}
					>
						<Image
							source={require('../../Assets/Images/map-location.png')}
							style={{ width: 25, height: 25 }}
						/>
					</View>
				</TouchableNativeFeedback>
				<TouchableNativeFeedback onPress={() => Actions.my_requests({ token })}>
					<View
						style={{
							width: 50,
							height: 50,
							borderRadius: 30,
							backgroundColor: '#fff',
							alignItems: 'center',
							justifyContent: 'center',
							borderColor: COLORS.SECONDARY,
							borderWidth: 1,
							position: 'absolute',
							bottom: 70,
							left: 20,
						}}
					>
						<Image
							source={require('../../Assets/Images/blood-donation.png')}
							style={{ width: 25, height: 25 }}
						/>
					</View>
				</TouchableNativeFeedback>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		width: 180,
		height: 50,
		borderRadius: 50,
		elevation: 3,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		bottom: 70,
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 12,
		fontWeight: '500',
		color: '#fff',
	},
});
