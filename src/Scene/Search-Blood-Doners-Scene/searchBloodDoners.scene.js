import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView, TouchableHighlight, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import moment from 'moment';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

import { AddRequests } from '../../api/private';

var t = require('tcomb-form-native');
var stylesheet = require('tcomb-form-native/lib/stylesheets/bootstrap');

var Form = t.form.Form;

t.form.Form.stylesheet = stylesheet;

// here we are: define your domain model
var Person = t.struct({
	bloodGroup: t.enums({
		'A+': 'A+',
		'A-': 'A-',
		'A1+': 'A1+',
		'A1-': 'A1-',
		'A1B+': 'A1B+',
		'A2+': 'A2+',
		'A2-': 'A2-',
		'A2B+': 'A2B+',
		'A2B-': 'A2B-',
		'AB+': 'AB+',
		'AB-': 'AB-',
		'B+': 'B+',
		'Bombay Blood Group': 'Bombay Blood Group',
		INRA: 'INRA',
		'O+': 'O+',
		'O-': 'O-',
	}),
	bloodUntis: t.Number,
	fullName: t.String,
	contactNumber: t.Number,
	hospitalLocationAddress: t.String,
	urgent: t.Boolean,
	requireUpto: t.Date,
});

var requireUpto = {
	label: 'Require upto',
	mode: 'date',
	config: {
		format: date => myFormatFunction('DD MMM YYYY', date),
	},
};

let options = {
	fields: {
		requireUpto: requireUpto,
	},
};

let myFormatFunction = (format, date) => {
	return moment(date).format(format);
};

const { width, height } = Dimensions.get('window');

export default class SearchBloodDoners extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
			value: {
				bloodGroup: 'A+',
				bloodUntis: '',
				fullName: '',
				contactNumber: '',
				urgent: false,
				requireUpto: null,
				hospitalLocationAddress: props.locationObject.formatted_address,
			},
		};
	}

	onPress = async () => {
		const { token } = this.props;
		// call getValue() to get the values of the form
		var value = this.refs.form.getValue();
		if (value) {
			value = this.processData(value);
			const result = await AddRequests({ payload: value }, token);
			if (result.success) {
				Actions.pop();
				Alert.alert('Success', 'Blood donation request is sent to doners nearby you.');
			}
		}
	};

	processData = value => {
		const { locationObject } = this.props;
		console.log('value', value);
		const returnObject = {
			name: value.fullName,
			blood_group: value.bloodGroup,
			units: value.bloodUntis,
			is_urgent: value.urgent,
			contact_number: value.contactNumber,
			latitude: locationObject.latitude,
			longitude: locationObject.longitude,
			address: locationObject.formatted_address,
			require_upto: value.requireUpto,
			locationObject: locationObject,
		};
		return returnObject;
	};

	onChange = value => {
		this.setState({ value });
	};

	render() {
		const { value } = this.state;

		return (
			<View style={styles.container}>
				<ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingLeft: 20, paddingRight: 20 }}>
					<Form ref="form" value={value} onChange={this.onChange} type={Person} options={options} />
				</ScrollView>
				<TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor="#99d9f4">
					<Text style={styles.buttonText}>Submit</Text>
				</TouchableHighlight>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		marginTop: 150,
	},
	submitButton: {
		paddingHorizontal: 10,
		paddingTop: 20,
	},
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	buttonText: {
		fontSize: 18,
		color: 'white',
		alignSelf: 'center',
	},
	button: {
		height: 50,
		backgroundColor: COLORS.SECONDARY,
		alignSelf: 'stretch',
		justifyContent: 'center',
	},
});
