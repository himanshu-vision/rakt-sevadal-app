import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

const { width, height } = Dimensions.get('window');

export default class AboutusScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
		};
	}

	renderItem = (icon, text, action) => {
		return (
			<View
				style={{
					width: width - 50,
					minHeight: 50,
					padding: 10,
					marginTop: 5,
					flexDirection: 'row',
				}}
			>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
					<Image source={icon} style={{ width: 25, height: 25, resizeMode: 'contain' }} />
				</View>
				<View style={{ flex: 4, justifyContent: 'center', alignItems: 'flex-start' }}>
					<Text style={{ fontWeight: '300', fontSize: 16 }}>{text}</Text>
				</View>
			</View>
		);
	};

	render() {
		const { latitude, longitude } = this.state;

		return (
			<View style={styles.container}>
				<View
					style={{
						width,
						height: height / 3,
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					<Image
						source={require('../../Assets/Images/logo.jpeg')}
						style={{ width: 120, height: 120, padding: 20, borderRadius: 50, resizeMode: 'contain' }}
					/>
					<Text style={{ fontWeight: '800', color: COLORS.SECONDARY, fontSize: 24 }}>Rakt Sevadal</Text>
				</View>
				<View style={{ width: width - 30, height: 2 * height / 3 }}>
					<Text style={{ fontWeight: '500', fontSize: 18, color: COLORS.SECONDARY, padding: 20 }}>
						Motivation
					</Text>
					<Text>
						Rakt Sevadal: Blood Donation App helps people that are desperately in need of blood, Each that
						are in position to help/donate blood. Each year millions of people lose their life because they
						do not find donors when needed most. It willing to help everywhere but they are not sure
						when/where an actua need arise. Rakt Sevadal is solution as a platform where patients can
						instantly connect for help and donors can respond instantly.
					</Text>
				</View>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
