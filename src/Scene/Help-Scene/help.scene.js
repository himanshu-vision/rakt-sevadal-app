import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableNativeFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

const { width, height } = Dimensions.get('window');

export default class HelpScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	renderCard = (image, text, onPress = () => {}) => {
		return (
			<TouchableNativeFeedback onPress={onPress}>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
					<Image style={{ width: 40, height: 40, resizeMode: 'contain' }} source={image} />
					<Text style={{ padding: 10, fontWeight: '500', color: '#de2430' }}>{text}</Text>
				</View>
			</TouchableNativeFeedback>
		);
	};

	render() {
		const { latitude, longitude } = this.state;

		return (
			<View style={styles.container}>
				<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
					{this.renderCard(require('../../Assets/Images/about.png'), 'About us', () => Actions.about_us())}
					{/* {this.renderCard(require('../../Assets/Images/helpline.png'), 'Helpline number', () =>
						Actions.know_blood_group()
					)} */}
				</View>
				<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
					{this.renderCard(require('../../Assets/Images/blood-test.png'), 'Know your blood group', () =>
						Actions.know_blood_group()
					)}
					{/* {this.renderCard(require('../../Assets/Images/share.png'), 'Share')} */}
				</View>
				{/* <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
					{this.renderCard(require('../../Assets/Images/hospital.png'), 'Nearest hospitals')}
					{this.renderCard(require('../../Assets/Images/blood-bank.png'), 'Nearest blood bank')}
				</View> */}
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
