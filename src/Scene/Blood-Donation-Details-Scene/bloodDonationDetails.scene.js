import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView, Linking, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';
import moment from 'moment';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

import { GetDetailsOfRequestApiCall } from '../../api/private';

const { width, height } = Dimensions.get('window');

export default class BloodDonationDetailsScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
			data: null,
		};
	}

	componentDidMount = async () => {
		const { id, token } = this.props;

		const result = await GetDetailsOfRequestApiCall(id, token);
		if (result.success) {
			this.setState({ data: result.response });
		}
	};

	callNumber = url => {
		Linking.canOpenURL(url)
			.then(supported => {
				if (!supported) {
					console.log("Can't handle url: " + url);
				} else {
					return Linking.openURL(url);
				}
			})
			.catch(err => console.error('An error occurred', err));
	};

	renderItem = item => {
		return (
			<View
				style={{
					width,
					minHeight: 50,
					padding: 10,
					marginTop: 5,
					flexDirection: 'row',
					elevation: 3,
					backgroundColor: '#fff',
				}}
			>
				<View style={{ flex: 4, justifyContent: 'center', alignItems: 'flex-start' }}>
					<Text style={{ fontSize: 16, fontWeight: '500' }}>
						{item.display_name}
						<Text style={{ fontSize: 16, fontWeight: '500', color: COLORS.SECONDARY }}>
							{' '}{item.blood_group}
						</Text>
					</Text>
					<Text style={{ fontSize: 14 }}>
						{item.gender}, Age: {item.age}
					</Text>
					<Text style={{ fontSize: 16, fontWeight: '500' }}>{item.city}</Text>
					<Text>Contact details - </Text>
					<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
						<TouchableOpacity
							style={{ padding: 10 }}
							onPress={() => this.callNumber(`tel:+91${item.alternate_contact}`)}
						>
							<Text style={{ fontSize: 16, fontWeight: '500' }}>{item.alternate_contact}</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={{ padding: 10 }}
							onPress={() => this.callNumber(`tel:+91${item.mobile}`)}
						>
							<Text style={{ fontSize: 16, fontWeight: '500' }}>{item.mobile}</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	};

	render() {
		const { data } = this.state;
		if (!data) return null;
		return (
			<View style={styles.container}>
				<View
					style={{
						width: width,
						height: height / 3,
						backgroundColor: COLORS.SECONDARY,
						alignItems: 'center',
						justifyContent: 'center',
						elevation: 10,
					}}
				>
					<Text style={{ color: '#fff', style: width - 30 }}>
						<Text style={{ fontWeight: '500', color: '#fff' }}>{data.name} </Text>
						<Text style={{ color: '#fff' }}>Requires {data.units} units of </Text>
						<Text style={{ fontWeight: '700', color: '#fff' }}> {data.blood_group} </Text> Blood Group at{' '}
						{data.address}
					</Text>
					<Text style={{ fontWeight: '200', color: '#fff', margin: 2 }}>
						Requires Upto: {moment(data.require_upto).format('DD MMM YYYY')}{' '}
					</Text>
				</View>
				<View style={{ width, height: 2 * height / 3 }}>
					<Text style={{ padding: 5, fontSize: 18 }}>Peoples who accepted request</Text>
					<ScrollView
						style={{ flex: 1 }}
						contentContainerStyle={{ alignItems: 'center', paddingBottom: 100 }}
					>
						{data.accepted.map(item => {
							return this.renderItem(item);
						})}
					</ScrollView>
				</View>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
