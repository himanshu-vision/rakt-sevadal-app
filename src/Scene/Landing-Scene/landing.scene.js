import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView, ActivityIndicator, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';
import { BackHandler } from 'react-native';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

import { GetBasicDetails } from '../../api/private';

const { width, height } = Dimensions.get('window');

export default class ProfileScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
		};
		this.checkIsLocation().catch(error => error);

		BackHandler.addEventListener('hardwareBackPress', () => {
			//(optional) you can use it if you need it
			LocationServicesDialogBox.forceCloseDialog();
		});
	}

	componentDidMount = async () => {
		const token = await AsyncStorage.getItem('@MySuperStore:TOKEN');
		setTimeout(async () => {
			if (token) {
				const result = await GetBasicDetails(token);
				if (result.success) {
					Actions.popAndPush('tab', { token });
				} else {
					AsyncStorage.removeItem('@MySuperStore:TOKEN');
					Actions.popAndPush('login');
				}
			} else {
				Actions.popAndPush('login');
			}
		}, 1000);
	};

	async checkIsLocation() {
		let check = await LocationServicesDialogBox.checkLocationServicesIsEnabled({
			message:
				'<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>',
			ok: 'YES',
			cancel: 'NO',
			enableHighAccuracy: false, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
			showDialog: true, // false => Opens the Location access page directly
			openLocationServices: true, // false => Directly catch method is called if location services are turned off
			preventOutSideTouch: false, //true => To prevent the location services window from closing when it is clicked outside
			preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
		}).catch(error => {
			BackHandler.exitApp();
		});

		return Object.is(check.status, 'enabled');
	}

	render() {
		const { latitude, longitude } = this.state;

		return (
			<View style={styles.container}>
				<View
					style={{
						flex: 1,
						backgroundColor: '#fff',
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					<Image
						source={require('../../Assets/Images/logo.jpeg')}
						style={{ width: 150, height: 150, margin: 20, resizeMode: 'contain' }}
					/>
					<ActivityIndicator size="large" color={COLORS.SECONDARY} />
				</View>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
