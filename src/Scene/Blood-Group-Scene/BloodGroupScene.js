import React, { Component } from 'react';
import _ from 'lodash';
import { ScrollView, Text, StyleSheet, Button, Image, Keyboard, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import InputBox from '../../Components/Input-Box/InputBox';
import { Icons } from 'react-native-fontawesome';
import COLORS from '../../Constants/color';
BLOOD_GROUPS = [
	'A+',
	'A-',
	'B+',
	'B-',
	'O+',
	'O-',
	'AB+',
	'AB-',
	'A1+',
	'A1-',
	'A1B+',
	'A1B-',
	'A2+',
	'A2-',
	'A2B+',
	'A2B-',
	'BBG',
	'INRA',
];

import { UpdateBloodGroupCall } from '../../api/private';

class BloodGroupScene extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bloodGroup: BLOOD_GROUPS[0],
			isProcessing: false,
		};
	}

	submit = async () => {
		const { bloodGroup } = this.state;
		const { token } = this.props;

		const body = {
			blood_group: bloodGroup,
		};

		const result = await UpdateBloodGroupCall(body, token);
		if (result.success) {
			Actions.tab({ token });
		}
	};

	RenderButton = bloodGroup => {
		return (
			<TouchableOpacity
				onPress={() => this.setState({ bloodGroup })}
				style={{
					flex: 1,
					backgroundColor: '#fff',
					justifyContent: 'center',
					alignItems: 'center',
					borderWidth: bloodGroup == this.state.bloodGroup ? 4 : 1,
					borderColor: COLORS.SECONDARY,
					margin: 10,
				}}
			>
				<Text style={{ color: COLORS.PRIMARY, fontSize: 24, fontWeight: '300' }}>{bloodGroup}</Text>
			</TouchableOpacity>
		);
	};

	render() {
		const { bottomStickButtonVisible, isProcessing } = this.state;
		const { name, age, gender, alternateContact } = this.state;

		return (
			<View style={{ flex: 1 }}>
				<ScrollView contentContainerStyle={styles.container}>
					{_.chunk(BLOOD_GROUPS, 3).map((row, key1) => {
						return (
							<View key={key1} style={{ width: 340, flexDirection: 'row', height: 100 }}>
								{row.map(bg => this.RenderButton(bg))}
							</View>
						);
					})}
				</ScrollView>
				<LinearGradientButton isProcessing={isProcessing} onPress={() => this.submit()} title={'Proceed'} />
			</View>
		);
	}
}

export default BloodGroupScene;

var styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		padding: 20,
		alignItems: 'center',
		justifyContent: 'center',
		paddingBottom: 100,
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	inputContainerStyle: {
		margin: 8,
	},
});
