import React, { Component } from 'react';
import _ from 'lodash';
import { ScrollView, Text, StyleSheet, Button, Image, Keyboard, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import InputBox from '../../Components/Input-Box/InputBox';
import { Icons } from 'react-native-fontawesome';
import COLORS from '../../Constants/color';

import { UpdateGenderCall } from '../../api/private';

GENDERS = ['Male', 'Female', 'Third Gender'];

class UserGenderScene extends Component {
	constructor(props) {
		super(props);
		this.state = {
			gender: GENDERS[0],
			isProcessing: false,
		};
	}

	submit = async () => {
		const { gender } = this.state;
		const { token } = this.props;

		const body = {
			gender,
		};
		const result = await UpdateGenderCall(body, token);
		if (result.success) {
			Actions.blood_group({ token });
		}
	};

	RenderButton = gender => {
		return (
			<TouchableOpacity
				onPress={() => this.setState({ gender })}
				style={{
					flex: 1,
					backgroundColor: '#fff',
					justifyContent: 'center',
					alignItems: 'center',
					borderWidth: gender == this.state.gender ? 4 : 1,
					borderColor: COLORS.SECONDARY,
					margin: 10,
				}}
			>
				<Text style={{ color: COLORS.PRIMARY, fontSize: 24, fontWeight: '300' }}>{gender}</Text>
			</TouchableOpacity>
		);
	};

	render() {
		const { bottomStickButtonVisible, isProcessing } = this.state;

		return (
			<ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
				{_.chunk(GENDERS, 2).map(row => {
					return (
						<View style={{ width: 340, flexDirection: 'row', height: 100 }}>
							{row.map(bg => {
								return this.RenderButton(bg);
							})}
						</View>
					);
				})}
				<LinearGradientButton isProcessing={isProcessing} onPress={() => this.submit()} title={'Proceed'} />
			</ScrollView>
		);
	}
}

export default UserGenderScene;

var styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		padding: 20,
		alignItems: 'center',
		justifyContent: 'center',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	inputContainerStyle: {
		margin: 8,
	},
});
