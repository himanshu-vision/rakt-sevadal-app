import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

const { width, height } = Dimensions.get('window');

export default class ProfileScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
		};
	}

	renderItem = (icon, text, action) => {
		return (
			<View
				style={{
					width: width - 50,
					minHeight: 50,
					padding: 10,
					marginTop: 5,
					flexDirection: 'row',
				}}
			>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
					<Image source={icon} style={{ width: 25, height: 25, resizeMode: 'contain' }} />
				</View>
				<View style={{ flex: 4, justifyContent: 'center', alignItems: 'flex-start' }}>
					<Text style={{ fontWeight: '300', fontSize: 16 }}>{text}</Text>
				</View>
			</View>
		);
	};

	render() {
		const { latitude, longitude } = this.state;

		return (
			<View style={styles.container}>
				<View
					style={{
						width,
						height: height / 3,
						backgroundColor: COLORS.SECONDARY,
						alignItems: 'center',
						justifyContent: 'center',
						elevation: 10,
					}}
				>
					<Image
						source={require('../../Assets/Images/profile-male.png')}
						style={{ width: 60, height: 60, margin: 20 }}
					/>
					<Text style={{ fontWeight: '800', color: '#fff', fontSize: 24 }}>Himanshu Kushwah</Text>
				</View>
				<View style={{ width, height: 2 * height / 3 }}>
					<ScrollView style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center', paddingBottom: 100 }}>
						{this.renderItem(require('../../Assets/Images/mobile-phone.png'), '+91 8562843744')}
						{this.renderItem(require('../../Assets/Images/age.png'), '23')}
						{this.renderItem(require('../../Assets/Images/genders.png'), 'Male')}
						{this.renderItem(require('../../Assets/Images/blood-drop.png'), 'O+')}
						{this.renderItem(
							require('../../Assets/Images/map-location.png'),
							'1, 1st Cross Rd, Vysya Bank Colony, Stage, 2 BTM 2nd Stage, Bengaluru, Karnataka 560076, India, Bengalutu, Indai'
						)}
						{/* {this.renderItem(require('../../Assets/Images/pencil.png'), 'Edit profile')} */}
					</ScrollView>
				</View>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
