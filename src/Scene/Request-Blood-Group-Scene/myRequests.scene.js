import React from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';

import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

import BloodDonationRequestCard from './Components/bloodDonationRequestCard.component';

import { FetchMyRequests } from '../../api/private';

const { width, height } = Dimensions.get('window');

export default class MyRequestBloodDonerScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			list: [],
			refreshing: false,
		};
	}

	renderCard = (item, key) => (
		<BloodDonationRequestCard
			onPress={() => Actions.request_detail({ id: item._id, token: this.props.token })}
			token={this.props.token}
			hideButton={true}
			data={item}
			key={key}
		/>
	);

	componentDidMount = async () => {
		const { token } = this.props;
		const { refreshing } = this.state;
		this.setState({ refreshing: true });
		const result = await FetchMyRequests(token);
		console.log(result);
		this.setState({ refreshing: false });
		if (result.success) {
			this.setState({ list: result.response });
		}
	};

	render() {
		const { list, refreshing } = this.state;

		return (
			<View style={styles.container}>
				<FlatList
					style={{ flex: 1 }}
					data={list.reverse()}
					renderItem={({ item, key }) => this.renderCard(item, key)}
					contentContainerStyle={{ paddingBottom: 30 }}
					showsVerticalScrollIndicator={false}
					onRefresh={() => this.componentDidMount()}
					refreshing={refreshing}
				/>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
