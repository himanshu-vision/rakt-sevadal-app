import React from 'react';
import { View, Image, Text, TouchableOpacity, Dimensions, Platform, Linking } from 'react-native';
import moment from 'moment';

const { width, height } = Dimensions.get('window');
import COLORS from '../../../Constants/color';

import { AcceptRequestCall } from '../../../api/private';

function openMap(lat, lng, name) {
	const scheme = Platform.OS === 'ios' ? 'maps:0,0?q=' : 'geo:0,0?q=:';
	const latLng = `${lat},${lng}`;
	const label = name;
	const url = Platform.OS === 'ios' ? `${scheme}@${latLng}` : `${scheme}${latLng}`;

	Linking.openURL(url);
}

const BloodDonationRequestCard = ({ data, token, hideButton, onPress }) => (
	<TouchableOpacity
		onPress={() => {
			if (onPress && typeof onPress == 'function') {
				onPress();
			}
		}}
	>
		<View
			style={{
				width: width - 10,
				elevation: 2,
				backgroundColor: '#fff',
				alignItems: 'center',
				justifyContent: 'center',
				margin: 5,
				borderRadius: 2,
				marginBottom: 10,
			}}
		>
			<View style={{ flex: 1, padding: 15 }}>
				<Text style={{}}>
					<Text style={{ fontWeight: '500' }}>{data.name} </Text>
					<Text>Requires {data.units} units of </Text>
					<Text style={{ fontWeight: '700', color: COLORS.SECONDARY }}> {data.blood_group} </Text> Blood Group
					at {data.address}
				</Text>
				<Text style={{ fontWeight: '200', color: '#26A65B', margin: 2 }}>
					Requires Upto: {moment(data.require_upto).format('DD MMM YYYY')}{' '}
				</Text>
			</View>
			{!hideButton ? (
				<View
					style={{
						width,
						height: 50,
						flexDirection: 'row',
					}}
				>
					<TouchableOpacity
						onPress={async () => {
							const result = await AcceptRequestCall(data._id, token);
							if (result.success) {
								alert(
									'Blood Donation Request Accepted. Blood Requester will be notified with your details'
								);
							} else {
								alert('something went wrong try again');
							}
						}}
						style={{
							flex: 2,
							flexDirection: 'row',
							justifyContent: 'center',
							alignItems: 'center',
							borderBottomLeftRadius: 10,
						}}
					>
						<Image
							style={{ width: 25, height: 25, resizeMode: 'contain', margin: 10 }}
							source={require('../../../Assets/Images/donation.png')}
						/>
						<Text style={{ fontWeight: '500', color: COLORS.SECONDARY }}>Accept</Text>
					</TouchableOpacity>

					{/* <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
				<Image
					style={{ width: 25, height: 25, resizeMode: 'contain', margin: 5 }}
					source={require('../../../Assets/Images/share.png')}
				/>
			</View> */}

					<TouchableOpacity
						onPress={() => openMap(data.latitude, data.longitude, data.name)}
						style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
					>
						<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
							<Image
								style={{ width: 25, height: 25, resizeMode: 'contain', margin: 5 }}
								source={require('../../../Assets/Images/map-location.png')}
							/>
						</View>
					</TouchableOpacity>
				</View>
			) : null}
		</View>
	</TouchableOpacity>
);

export default BloodDonationRequestCard;
