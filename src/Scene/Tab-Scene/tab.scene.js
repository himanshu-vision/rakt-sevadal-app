import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import MapView from 'react-native-maps';
import TabNavigator from 'react-native-tab-navigator';

// import LocationService from '../../Utils/location.service';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import COLORS from '../../Constants/color';

import HomeScene from '../Home-Scene/Home.scene';
import ProfileScene from '../Profile-Scene/profile.scene';
import HelpScene from '../Help-Scene/help.scene';
import RequestBloodGroupScene from '../Request-Blood-Group-Scene/requestBloodGroup.scene';

const { width, height } = Dimensions.get('window');

export default class TabScene extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isProcessing: true,
			selectedTab: 'home',
		};
	}

	render() {
		const { latitude, longitude } = this.state;
		const { token } = this.props;

		return (
			<TabNavigator
				style={styles.container}
				tabBarStyle={{ height: 70, justifyContent: 'center', alignItems: 'center' }}
			>
				<TabNavigator.Item
					selected={this.state.selectedTab === 'home'}
					title="Home"
					renderIcon={() => (
						<Image
							style={{ width: 35, height: 35, resizeMode: 'contain' }}
							source={require('../../Assets/Images/search.png')}
						/>
					)}
					renderSelectedIcon={() => (
						<Image
							style={{ width: 37, height: 37, resizeMode: 'contain' }}
							source={require('../../Assets/Images/search.png')}
						/>
					)}
					onPress={() => this.setState({ selectedTab: 'home' })}
				>
					<HomeScene token={token} />
				</TabNavigator.Item>
				<TabNavigator.Item
					selected={this.state.selectedTab === 'requests'}
					title="Requests"
					renderIcon={() => (
						<Image
							style={{ width: 35, height: 35, resizeMode: 'contain' }}
							source={require('../../Assets/Images/donation.png')}
						/>
					)}
					renderSelectedIcon={() => (
						<Image
							style={{ width: 37, height: 37, resizeMode: 'contain' }}
							source={require('../../Assets/Images/donation.png')}
						/>
					)}
					onPress={() => this.setState({ selectedTab: 'requests' })}
				>
					<RequestBloodGroupScene token={token} />
				</TabNavigator.Item>
				<TabNavigator.Item
					selected={this.state.selectedTab === 'profile'}
					title="Profile"
					renderIcon={() => (
						<Image
							style={{ width: 35, height: 35, resizeMode: 'contain' }}
							source={require('../../Assets/Images/profile-male.png')}
						/>
					)}
					renderSelectedIcon={() => (
						<Image
							style={{ width: 37, height: 37, resizeMode: 'contain' }}
							source={require('../../Assets/Images/profile-male.png')}
						/>
					)}
					onPress={() => this.setState({ selectedTab: 'profile' })}
				>
					<ProfileScene token={token} />
				</TabNavigator.Item>
				<TabNavigator.Item
					selected={this.state.selectedTab === 'help'}
					title="Help"
					renderIcon={() => (
						<Image
							style={{ width: 35, height: 35, resizeMode: 'contain' }}
							source={require('../../Assets/Images/help.png')}
						/>
					)}
					renderSelectedIcon={() => (
						<Image
							style={{ width: 37, height: 37, resizeMode: 'contain' }}
							source={require('../../Assets/Images/help.png')}
						/>
					)}
					onPress={() => this.setState({ selectedTab: 'help' })}
				>
					<HelpScene token={token} />
				</TabNavigator.Item>
			</TabNavigator>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	map: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width,
		height,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	buttonCircle: {
		position: 'absolute',
		bottom: 25,
		width: 250,
		height: 70,
		borderRadius: 35,
		elevation: 3,
		left: width / 5,
		flexDirection: 'row',
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		borderTopStartRadius: 35,
		borderBottomStartRadius: 35,
	},
	rightButton: {
		flex: 1,
		backgroundColor: COLORS.PRIMARY,
		justifyContent: 'center',
		alignItems: 'center',
		borderTopEndRadius: 35,
		borderBottomEndRadius: 35,
	},
	textOne: {
		fontSize: 18,
		fontWeight: '600',
		color: '#fff',
	},
});
