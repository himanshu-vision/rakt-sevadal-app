
import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet, Button, Image, Keyboard, View } from "react-native";
import { Actions } from "react-native-router-flux";
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import InputBox from '../../Components/Input-Box/InputBox';
import { Icons } from 'react-native-fontawesome';

import { UpdateProfileCall, GetBasicDetails } from '../../api/private';

MODES = ['MOBILE_NUMBER_INPUT', 'OTP_INPUT'];
GENDER = ['MALE', 'FEMALRE'];

class UserDetailsScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            age: '',
            alternateContact: '',
            bottomStickButtonVisible: true,
            isProcessing: false,
        }
    }

    componentDidMount = () => {
        this.fetchBasicData();
    }

    fetchBasicData = async () => {
        const { token } = this.props;
        const result = await GetBasicDetails(token);
        if (result.success) {
            this.setState({
                name: result.response.display_name,
                age: result.response.age,
                alternateContact: result.response.alternate_contact
            });
        }
    }

    componentWillMount = () => {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount = () => {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow = () => {
        this.setState({ bottomStickButtonVisible: false });
    }

    _keyboardDidHide = () => {
        this.setState({ bottomStickButtonVisible: true });
    }

    submit = async () => {
        const { name, age, alternateContact } = this.state;
        const { token } = this.props;

        body = {
            full_name: name,
            age,
            alt_contact: alternateContact,
        }
        this.setState({ isProcessing: true });
        const result = await UpdateProfileCall(body, token);
        this.setState({ isProcessing: false });
        if (result.success) {
            Actions.gender({ token });
        }
    }

    render() {
        const { bottomStickButtonVisible, isProcessing } = this.state;
        const { name, age, alternateContact, } = this.state;

        return (
            <ScrollView
                style={{ flex: 1 }}
                contentContainerStyle={styles.container}
            >
                <View style={{ marginTop: 50 }}>
                    <InputBox
                        value={name}
                        placeholder={'Enter your full name'}
                        icon={Icons.user}
                        onChangeText={(name) => this.setState({ name })}
                    />

                    <InputBox
                        value={age}
                        placeholder={'Enter your age'}
                        icon={Icons.gift}
                        keyboardType={'numeric'}
                        maxLength={3}
                        onChangeText={(age) => this.setState({ age })}
                    />

                    <InputBox
                        value={alternateContact}
                        placeholder={'Enter alternate contact number'}
                        icon={Icons.mobile}
                        keyboardType={'numeric'}
                        maxLength={10}
                        onChangeText={(alternateContact) => this.setState({ alternateContact })}
                    />
                </View>

                {
                    bottomStickButtonVisible &&
                    <LinearGradientButton
                        isProcessing={isProcessing}
                        onPress={() => this.submit()}
                        title={'Proceed'}
                    />
                }
            </ScrollView>
        );
    }
}

export default UserDetailsScene;

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 20,
        alignItems: 'center',
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5,
    },
    inputContainerStyle: {
        margin: 8,
    },
});
