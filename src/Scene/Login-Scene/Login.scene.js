import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet, Button, Image, Keyboard, View, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MessageBar, MessageBarManager } from 'react-native-message-bar';
import LinearGradientButton from '../../Components/LinearGradientButton/Linerar-Gradient-Button';
import InputBox from '../../Components/Input-Box/InputBox';
import { Icons } from 'react-native-fontawesome';

import { GenerateOTPCall, VerifyOTPCall } from '../../api/private';

MODES = ['MOBILE_NUMBER_INPUT', 'OTP_INPUT'];

class LoginScene extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mobile: '',
			otp: '',
			bottomStickButtonVisible: true,
			mode: MODES[0],
			isProcessing: false,
		};
	}

	componentWillMount = () => {
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
	};

	componentWillUnmount = () => {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	};

	_keyboardDidShow = () => {
		this.setState({ bottomStickButtonVisible: false });
	};

	_keyboardDidHide = () => {
		this.setState({ bottomStickButtonVisible: true });
	};

	RenderForm = () => {
		const { mode } = this.state;
		switch (mode) {
			case MODES[0]:
				return this.RenderMobileInputForm();
				break;
			case MODES[1]:
				return this.RenderOTPInputForm();
				break;
			default:
				return null;
		}
	};

	RenderMobileInputForm = () => {
		const { mobile, mode } = this.state;
		return (
			<InputBox
				value={mobile}
				placeholder={'Enter mobile number'}
				icon={Icons.mobile}
				keyboardType={'numeric'}
				maxLength={10}
				onChangeText={mobile => this.setState({ mobile })}
			/>
		);
	};

	RenderOTPInputForm = () => {
		const { mobile, otp, isProcessing, mode } = this.state;
		return (
			<View>
				<InputBox
					value={mobile}
					placeholder={'Enter mobile number'}
					icon={Icons.user}
					keyboardType={'numeric'}
					maxLength={10}
					onChangeText={mobile => this.setState({ mobile })}
					editable={mode != MODES[0]}
				/>
				<InputBox
					value={otp}
					placeholder={'Enter One Time Password'}
					icon={Icons.lock}
					keyboardType={'numeric'}
					maxLength={4}
					onChangeText={otp => this.setState({ otp })}
				/>
			</View>
		);
	};

	submit = async () => {
		const { mode, mobile, otp } = this.state;
		switch (mode) {
			case MODES[0]:
				this.setState({ isProcessing: true });
				const result1 = await GenerateOTPCall({ mobile });
				if (result1.success) {
					MessageBarManager.showAlert({
						title: 'OTP Sent',
						message: 'OTP has been sent succefully to your mobile number',
						alertType: 'success',
					});
					this.setState({ isProcessing: false, mode: MODES[1] });
				}
				break;
			case MODES[1]:
				this.setState({ isProcessing: true });
				const result2 = await VerifyOTPCall({ mobile, otp });

				if (result2.success) {
					MessageBarManager.showAlert({
						title: 'OTP Verified',
						alertType: 'success',
					});
					this.setState({ isProcessing: false });
					this.storeToken(result2.token);
					Actions.user_details({ token: result2.token });
				}
				break;
			default:
				return null;
		}
	};

	storeToken = async token => {
		try {
			await AsyncStorage.setItem('@MySuperStore:TOKEN', String(token));
		} catch (e) {
			console.log('ROORO', e);
		}
	};

	render() {
		const { mode, bottomStickButtonVisible, isProcessing } = this.state;

		return (
			<ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
				<Image
					source={require('../../Assets/Images/logo.jpeg')}
					style={{
						width: 250,
						height: 250,
						resizeMode: 'contain',
					}}
				/>
				{this.RenderForm()}
				{bottomStickButtonVisible && (
					<LinearGradientButton
						isProcessing={isProcessing}
						onPress={() => this.submit()}
						title={mode == MODES[0] ? 'Send OTP' : 'Verify OTP'}
					/>
				)}
			</ScrollView>
		);
	}
}

export default LoginScene;

var styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		padding: 20,
		alignItems: 'center',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	inputContainerStyle: {
		margin: 8,
	},
});
