export default {
	GenerateOTP: 'users/generateOTP',
	VerifyOTP: 'users/verifyOTP',
	UpdateBaseDetails: 'users/update/basic',
	UserBasicDetails: 'users/basic',
	UpdateGender: 'users/update/gender',
	UpdateBloodGroup: 'users/update/blood_group',
	UpdateUserLocation: 'users/update/location',
	AddRequests: 'request/add',
	FetchRequests: 'request/fetch',
    FetchMyRequests: 'request/fetchMine',
    GetDetailsOfRequest: 'request/details', // append req id
	AcceptRequest: 'request/accept', //append request id
};
