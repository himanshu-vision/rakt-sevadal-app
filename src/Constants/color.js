export default {
	PRIMARY: '#D31027',
	SECONDARY: '#dc2430',
	GRADIENT: ['#dc2430', '#7b4397'],
	FONT: '#FFFFFF',
	RED_COLOR: 'rgb(255,84,74)',
};
