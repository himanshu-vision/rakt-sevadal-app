import API from '../Constants/api';
import Http from '../Utils/http.util';

export function GenerateOTPCall(body) {
	return Http.post({ url: API.GenerateOTP, body });
}

export function VerifyOTPCall(body) {
	return Http.post({ url: API.VerifyOTP, body });
}

export function UpdateProfileCall(body, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: API.UpdateBaseDetails, body, headers });
}

export function GetBasicDetails(token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.get({ url: API.UserBasicDetails, headers });
}

export function UpdateGenderCall(body, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: API.UpdateGender, body, headers });
}

export function UpdateBloodGroupCall(body, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: API.UpdateBloodGroup, body, headers });
}

export function UpdateUserLocationCall(body, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: API.UpdateUserLocation, body, headers });
}

export function AddRequests(body, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: API.AddRequests, body, headers });
}

export function FetchRequests(token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.get({ url: API.FetchRequests, headers });
}

export function FetchMyRequests(token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.get({ url: API.FetchMyRequests, headers });
}

export function AcceptRequestCall(id, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.post({ url: `${API.AcceptRequest}/${id}`, headers });
}

// GetDetailsOfRequest

export function GetDetailsOfRequestApiCall(id, token) {
	const headers = {
		'x-access-token': token,
	};
	return Http.get({ url: `${API.GetDetailsOfRequest}/${id}`, headers });
}
