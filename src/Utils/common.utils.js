import _ from 'lodash';
import moment from 'moment';

/*
Implements utility functions to be used across project
*/

// let online = true;
/**
 * Check for internet connection
 * @todo as of now method returns true, implement method properly
 */
export function CheckInternet() {
    return true;
}

// /**
//  * Calculate Distance Between Two Lat lngg Points
//  * @param  {int} lat1
//  * @param  {int} lng1
//  * @param  {int} lat2
//  * @param  {int} lng2
//  * @param  {string} unit - either k, or m. where k stands for kilometer and m for miles
//  */
// export function GetDistanceFromLatLonInKm(lat1, lng1, lat2, lng2, unit) {
//     if (!(lat1 && lng1 && lat2 && lng2)) {
//         return 0;
//     }
//     const deg2rad = (degrees) => degrees * (Math.PI / 180);
//     const rad2deg = (radians) => radians * (180 / Math.PI);

//     theta = lng1 - lng2;
//     dist = (Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))) + (Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta)));
//     dist = Math.acos(dist);
//     dist = rad2deg(dist);
//     miles = dist * 60 * 1.1515;
//     Unit = unit.toUpperCase();

//     if (Unit == 'K') {
//         return (miles * 1.609344);
//     } else if (Unit == 'M') {
//         return (miles * 1.609344 * 1000);
//     }
//     return miles;
// }

/**
 * helper function to chekc if String contains Number or Not
 */
export function IsNumeric(n) {
    const regex = /^[1-9][0-9]*$/;
    return n.match(regex) !== null;
}

/**
 * to check if first character of String is Capital
 * used in fare breakup 
 */
export function IsFirstCharacterCapital(word) {
    const regex = /^[A-Z]/;
    return regex.test(word);
}

/**
 * Function to convet date time to readable format eg. 23 May, 10:23 AM
 * @param  {String} date
 */
export function GetDateTimeInReadableFormat(date, ifNoDateMessage) {
    let response = '';
    if (moment(date, 'YYY-MM-DD HH:mm:ss')) {
        response = moment(date, 'YYY-MM-DD HH:mm:ss').format('DD MMM, h:mm A');
    }
    if (response === 'Invalid date') {
        response = ifNoDateMessage;
    }
    return response;
}

// /**
//  * Function to Remove attributes in object having null or undefined value
//  * @param {Object} params
//  */
// export function RemoveNullAttributes(params) {
//     const result = {};
//     for (const key in params) {
//         if (params.hasOwnProperty(key)) {
//             if (params[key] !== null) {
//                 result[key] = params[key];
//             }
//         }
//     }
//     return result;
// }

// /**
//  * Function to convert object to url string
//  * @param  {object} obj
//  */
// export function UrlStringifyObject(obj) {
//     if (!obj) {
//         return '';
//     }
//     let returnString = '?';
//     for (key in obj) {
//         returnString = returnString.concat(`&${key}=${obj[key]}`);
//     }
//     return returnString;
// }

/**
 * Flatd down object
 * @param  {Object} object
 */
export function FlattenObject(object) {
    const toReturn = {};

    for (const i in object) {
        if (!object.hasOwnProperty(i)) {
            continue;
        }

        if ((typeof object[i]) == 'object') {
            const flatObject = FlattenObject(object[i]);
            for (const x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) {
                    continue;
                }
                toReturn[x] = flatObject[x];
            }
        } else {
            toReturn[i] = object[i];
        }
    }
    return toReturn;
}

/**
 * Function uppercases the first characted and appends the rest 
 * @param {*} string 
 */
export function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Function returns an object with key value pairs using the array
 * @param {*} arr 
 * @param {*} key 
 * @param {*} value 
 */
export function getObject(arr, key, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i][key] == value) {
            return arr[i];
        }
    }
    return null;
}

// /**
//  * @param  {} styleObj Object containing style elements
//  * Function that takes in style object in various structures
//  * and returns a suitable object
//  */
// export function StyleObjectFilter(styleObj) {
//     if (!Array.isArray(styleObj) && styleObj && typeof styleObj == 'object') {
//         newStylesObj = { ...{}, ...styleObj };
//     } else {
//         newStylesObj = styleObj;
//     }
//     return newStylesObj;
// }

/**
 * returns matched option from array against mentioned attribute value(in case of array of objects)
 * or element(in case of plain array)
 * @param  {array} hayStack - array
 * @param  {} needle - value
 * @param  {string} element - attribute name
 * @param  {int} defaultElement - if element not found, returns element of default index
 */
export function SelectFromOptions(hayStack, needle, element, defaultElement) {
    /* eslint-disable */
    defaultElement = defaultElement || 0;
    const isArray = IsUndefinedOrNull(element);
    for (let i in hayStack) {
        if (isArray) {
            if (hayStack[i] == needle)
                return hayStack[i];
        } else {
            if (hayStack[i][element] == needle)
                return hayStack[i];
        }
    }
    const finalElement = hayStack[defaultElement];

    return finalElement || hayStack[0];
    /* eslint-enable */
}

/**
 * return boolean if given variable is undefined or null
 * @param  {} value
 */
export function IsUndefinedOrNull(value) {
    return value == null || value == '';
}

/**
 * return boolean if given variable is undefined only
 * @param  {} value
 */
export function IsUndefined(value) {
    return value == null || value === '';
}

/**
 * Converts object to array
 * @param  {object} obj
 */
export function ObjectToArray(obj) {
    if (!(obj && typeof obj == 'object')) {
        return obj;
    }
    const arr = _.values(obj);
    return arr;
}

/**
 * Moves particular element to last position
 * usage - ride detail page discount is moved to last using this method
 * @param  {Array} arr
 * @param  {string} key
 */
export function MoveToLastPosition(arr, key, keyValue) {
    /* eslint-disable */
    for (let index in arr) {
        if (Object.hasOwnProperty.call(arr[index], key)) {
            arr[index][key] == keyValue ? arr.push(arr.splice(index, 1)[0]) : 0;
        }
    }
    /* eslint-enble */
    return arr;
}

/**
 * Converts array to object
 * @param  {Array} array
 */
export function ArrayToObject(array, key) {
    const obj = {};
    if (!Array.isArray(array)) {
        return {};
    }
    array.forEach((element, index) => {
        if ((element && typeof element == 'object' && element[key])) {
            obj[element[key]] = element;
        }
    });
    return obj;
}

/**
 * Capitalize first letter of the string 
 * ex- aaaaaa is converted to Aaaaaa
 * @param  {string} string
 */
export function CapitalizeFirstLetter(string) {
    if (!(string && typeof string == 'string')) {
        return string;
    }
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// function fixLength(data) {
//     const d = data.length === 1 ? data : `0${data}`;
//     return d;
// }

/**
 * Function to convert price into indian numbering system
 * like 10000 will be 10,000
 * @param  {number} price can be float or integer
 */
export function DisplayPrice(price) {
    let number = price;
    if (IsUndefined(price)) {
        return price;
    }
    if (!isNaN(number)) {
        number = parseFloat(number);
        number = number.toFixed(2);
    }
    if (typeof number == 'number') {
        number = number.toString();
    }
    let afterPoint = '';

    if (number.indexOf('.') > 0) {
        afterPoint = number.substring(number.indexOf('.'), number.length);
    }
    number = Math.floor(number);
    number = number.toString();
    let lastThree = number.substring(number.length - 3);
    const otherNumbers = number.substring(0, number.length - 3);
    if (otherNumbers != '') {
        lastThree = ',' + lastThree;
    }
    let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
    return `${CURRENCY} ${res}`;
}

/**
 * Convert Array to Object
 * @param  {array} arr
 */
export function ToObject(arr) {
    let rv = {};
    for (let i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
    return rv;
}
