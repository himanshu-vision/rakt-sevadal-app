import { Platform, BackHandler } from 'react-native';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';

export default function LocationService({ onGetLocation, onError, onRejectLocationPermission }) {
	if (Platform.OS == 'android') {
		LocationServicesDialogBox.checkLocationServicesIsEnabled({
			message:
				"<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
			ok: 'YES',
			cancel: 'NO',
			enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
			showDialog: true, // false => Opens the Location access page directly
			openLocationServices: true, // false => Directly catch method is called if location services are turned off
			preventOutSideTouch: true, //true => To prevent the location services window from closing when it is clicked outside
			preventBackClick: true, //true => To prevent the location services popup from closing when it is clicked back button
		})
			.then(function(success) {
				GetCurrentLocation({ onGetLocation, onError });
			})
			.catch(error => {
				onRejectLocationPermission ? onRejectLocationPermission(error) : () => {};
			});
	} else {
		GetCurrentLocation({ onGetLocation, onError });
	}
}

function GetCurrentLocation({ onGetLocation, onError }) {
    alert(navigator.geolocation);
	navigator.geolocation.getCurrentPosition(position => onGetLocation(position), error => onError(error), {
		enableHighAccuracy: true,
		timeout: 20000,
		maximumAge: 1000,
	});
}
