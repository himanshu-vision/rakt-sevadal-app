import { AsyncStorage } from 'react-native';
import { IsUndefined } from '../Utils/common.utils';
import GLOBAL from '../Constants/global';

const defaultHeaders = {
	'Content-Type': 'application/json',
};
/*
Gateway for all the api calls
implements get, post, put, delete calls
*/
export default class HttpService {
	/**
	 * Get call implementation
	 * All get calls are made through this method
	 * @param  {object} obj - contains url, params(optional){proccessed and attached to url}, hideMessage(optional){default: false} -
	 * obj can have hideLoader to hide loader which is false bydefault and shows loader on every call
	 * if response comes as string, that is shown in notification bar bydefault, that can be hidden by sending hideMessage as true
	 * headers(optional)
	 */
	static async get(obj) {
		if (!(obj && obj.url)) {
			return false;
		}

		const params = await http.getNecessaryParams(obj);
		return http.ApiCall(params);
	}

	/**
	 * Post call implementation
	 * All post calls are made through this method
	 * @param  {object} obj - contains url, params(optional){proccessed and attached to url}, hideMessage(optional){default: false} -
	 * * obj can have hideLoader to hide loader which is false bydefault and shows loader on every call
	 * if response comes as string, that is shown in notification bar bydefault, that can be hidden by sending hideMessage as true
	 * headers(optional), body(optional)
	 */
	static async post(obj) {
		if (!(obj && obj.url)) {
			return false;
		}

		obj.method = 'POST';
		const params = await http.getNecessaryParams(obj);
		return http.ApiCall(params);
	}

	/**
	 * Put call implementation
	 * All put calls are made through this method
	 * @param  {object} obj - contains url, params(optional){proccessed and attached to url}, hideMessage(optional){default: false} -
	 * * obj can have hideLoader to hide loader which is false bydefault and shows loader on every call
	 * if response comes as string, that is shown in notification bar by default, that can be hidden by sending hideMessage as true
	 * headers(optional), body(optional)
	 */
	static async put(obj) {
		if (!(obj && obj.url)) {
			return false;
		}

		obj.method = 'PUT';
		const params = await http.getNecessaryParams(obj);
		return http.ApiCall(params);
	}

	/**
	 * Delete call implementation
	 * All delete calls are made through this method
	 * @param  {object} obj - contains url, params(optional){proccessed and attached to url}, hideMessage(optional){default: true} -
	 * obj can have hideLoader to hide loader which is true bydefault and does not show loader on delete call
	 * if response comes as string, that is shown in notification bar by default, that can be hidden by sending hideMessage as true
	 * headers(optional)
	 */
	static async delete(obj) {
		if (!(obj && obj.url)) {
			return false;
		}

		obj.method = 'DELETE';
		if (IsUndefined(obj.hideMessage)) {
			obj.hideMessage = true;
		}

		if (IsUndefined(obj.hideLoader)) {
			obj.hideLoader = obj.hideLoader || true;
		}

		const params = await http.getNecessaryParams(obj);
		return http.ApiCall(params);
	}

	/**
	 * final level method to make api call
	 * used for all kind of methods(get, put, post), except delete
	 * @param  {string} {url
	 * @param  {function} method
	 * @param  {object} headers
	 * @param  {function} resolve
	 * @param  {function} reject}
	 */
	async ApiCall({
		url,
		method,
		headers,
		body,
		resolve = http.defaultResolve,
		reject = http.defaultReject,
		params,
		hideMessage,
		hideLoader,
	}) {
		const postDict = {
			headers,
			method,
		};
        const token = await AsyncStorage.getItem('@MySuperStore:TOKEN');
        
		if (token) {
			headers['x-access-token'] = token;
		}
		if (body) {
			// if body is attached
			postDict.body = body;
		}
		return fetch(url, { headers, body, method, params, credentials: 'same-origin' })
			.then(response => {
				return response.json();
			})
			.then(response => resolve(response, hideMessage, hideLoader))
			.catch(error => reject(error, hideMessage, hideLoader));
	}

	/**
	 * prepares params for making api calls
	 * including headers, url, params, resolve, reject
	 * @param  {object} obj
	 */
	async getNecessaryParams(obj) {
		const url = http.createFinalUrl(obj);
		const method = obj.method || 'GET';
		const headers = await http.createHeader(obj);

		const resolve = obj.hasOwnProperty('resolve') ? obj.resolve : http.resolve;
		const reject = obj.hasOwnProperty('reject') ? obj.reject : http.reject;

		if (!obj.hideLoader && !obj.hasOwnProperty('resolve')) {
			// if hide loader is not true, start loader
			// LoaderManager.startLoader();
		}
		const responseObj = {
			url,
			method,
			headers,
			resolve,
			reject,
			hideMessage: obj.hideMessage || false,
		};

		if (obj.body) {
			responseObj.body = JSON.stringify(obj.body);
		}
		return responseObj;
	}

	/**
	 * takes params along with end point, adds with prefix url and return final url
	 * @param  {object} obj
	 */
	createFinalUrl(obj) {
		const url = `${GLOBAL.API}/${obj.url}`;
		return url;
	}

	/**
	 * takes extra headers(optional) and extend with default header
	 * @param  {object} obj
	 */
	async createHeader(obj) {
		const headers = defaultHeaders;

		// if headers are not passed
		if (!obj.headers) {
			return headers;
		}
		// extend default header options with one, passed with obj
		return { ...headers, ...obj.headers };
	}

	/**
	 * default method to pass through on each success api call
	 * @param  {object} response
	 */
	defaultResolve(response, hideMessage, hideLoader) {
		console.log('resolve', response);
		if (!hideLoader) {
			// stop loader
			// LoaderManager.endLoader();
		}
		// if response contains string, show same in message bar
		if (!hideMessage && response && typeof response == 'object' && typeof response.response == 'string') {
			const type = response.success ? 'success' : 'error';
			// MessageBarManager.showAlert({
			//     title: '',
			//     message: response.response,
			//     alertType: type,
			// });
		}
		return response;
	}

	/**
	 * default method to pass through on each failure api call
	 * @param  {object} response
	 */
	defaultReject(response, hideMessage, hideLoader) {
		console.log(response);
		if (!hideLoader) {
			// stop loader
			// LoaderManager.endLoader();
		}
		let message = null;
		// if response contains string, show same in message bar
		if (!hideMessage && response && typeof response == 'object') {
			if (response.response == 'string') {
				message = response.message;
			} else if (response.reason == 'string') {
				message = response.reason;
			}
			// if (message) {
			//     MessageBarManager.showAlert({
			//         title: '',
			//         message,
			//         alertType: 'error',
			//     });
			// }
		}
		return response;
	}
}

// constructor to use methods declared inside class
const http = new HttpService();
