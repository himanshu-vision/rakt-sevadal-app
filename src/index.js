import React from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
import {
	Scene,
	Router,
	Actions,
	Reducer,
	ActionConst,
	Overlay,
	Tabs,
	Modal,
	Drawer,
	Stack,
	Lightbox,
} from 'react-native-router-flux';
import HomeScene from './Scene/Home-Scene/Home.scene';
import LoginScene from './Scene/Login-Scene/Login.scene';
import UserDetailsScene from './Scene/User-Details-Scene/UserDetails.scene';
import BloodGroupScene from './Scene/Blood-Group-Scene/BloodGroupScene';
import GenderScene from './Scene/User-Gender-Scene/UserGenderScene';
import UserLocationScene from './Scene/User-Location-Scene/UserLocation.scene';
import TabScene from './Scene/Tab-Scene/tab.scene';
import HelpScene from './Scene/Help-Scene/help.scene';
import ProfileScene from './Scene/Profile-Scene/profile.scene';
import AboutusScene from './Scene/About-Us-Scene/aboutus.scene';
import KnowYourBloodGroupScene from './Scene/Know-Your-Blood-Group-Scene/knowYourBloodGroup.scene';
import SearchBloodGroupScene from './Scene/Search-Blood-Doners-Scene/searchBloodDoners.scene';
import LandingScene from './Scene/Landing-Scene/landing.scene';
import MyRequestBloodDonerScene from './Scene/Request-Blood-Group-Scene/myRequests.scene';
import BloodDonationDetailsScene from './Scene/Blood-Donation-Details-Scene/bloodDonationDetails.scene';
import MessageBar from './Components/MessageBar/MessageBar';
import ErrorModal from './Components/Modal/ErrorModal';
import DemoLightbox from './Components/LightBox/DemoLightBox';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignItems: 'center',
	},
	tabBarStyle: {
		backgroundColor: '#eee',
	},
	tabBarSelectedItemStyle: {
		backgroundColor: '#ddd',
	},
	NavbarStyle: {
		backgroundColor: 'rgba(0,0,0,0)',
		zIndex: 100,
		top: 0,
		left: 0,
		right: 0,
	},
});

const reducerCreate = params => {
	const defaultReducer = new Reducer(params);
	return (state, action) => {
		console.log('ACTION:', action);
		return defaultReducer(state, action);
	};
};

const getSceneStyle = () => ({
	backgroundColor: '#F5FCFF',
	shadowOpacity: 1,
	shadowRadius: 3,
});

// on Android, the URI prefix typically contains a host in addition to scheme
const prefix = Platform.OS === 'android' ? 'raktsevadal://raktsevadal/' : 'raktsevadal://';

const AppRoot = () => (
	<Router
		// createReducer={reducerCreate}
		getSceneStyle={getSceneStyle}
		uriPrefix={prefix}
	>
		<Overlay key="overlay">
			<Modal
				key="modal"
				hideNavBar
				transitionConfig={() => ({ screenInterpolator: CardStackStyleInterpolator.forFadeFromBottomAndroid })}
			>
				<Lightbox key="lightbox">
					<Stack key="root">
						<Scene key="landing" initial hideNavBar component={LandingScene} />
						<Scene key="tab" hideNavBar navigationBarStyle={styles.NavbarStyle} component={TabScene} />
						<Scene
							key="home"
							headerMode="screen"
							navigationBarStyle={styles.NavbarStyle}
							component={HomeScene}
							title="Home"
							type={ActionConst.REPLACE}
						/>
						<Scene key="login" hideNavBar component={LoginScene} title="Login" />
						<Scene key="user_details" component={UserDetailsScene} title="User details" />
						<Scene key="blood_group" component={BloodGroupScene} title="Select blood group" />
						<Scene key="gender" component={GenderScene} title="Select gender" />
						<Scene key="location" component={UserLocationScene} title="Getting your location" />
						<Scene key="about_us" component={AboutusScene} title="About us" />
						<Scene key="know_blood_group" component={KnowYourBloodGroupScene} title={'Know blood group'} />
						<Scene
							key="my_requests"
							component={MyRequestBloodDonerScene}
							title={'My blood donation requests'}
						/>
						<Scene
							key="search_blood_doner"
							component={SearchBloodGroupScene}
							title={'Search blood doners'}
						/>
						<Scene
							key="request_detail"
							component={BloodDonationDetailsScene}
							title={'Blood donation request'}
						/>
					</Stack>
					<Scene key="demo_lightbox" component={DemoLightbox} />
				</Lightbox>
				<Scene key="error" component={ErrorModal} />
			</Modal>
			<Scene component={MessageBar} />
		</Overlay>
	</Router>
);

export default AppRoot;

const TabIcon = props => {
	return <Text style={{ color: props.focused ? 'red' : 'black' }}>{props.title}</Text>;
};
